/**************************************************
 * Errors to Write
 * 
 * + Compiler Errors
 *     - Missing semicolon
 * + Logic Errors
 *     - Index out of bounds error
 * + Variable Scope
 *     - Unneccesary Globals
 * + Memory Mangement
 *     - Unfreed malloc'd variable
 *     - Unused Variables
 * + Safe Functions
 *     - strcpy
 *     - gets
 *     - strcat
 **************************************************/
#include <string.h>;
#include <stdlib.h>
#include <stdio.h>
int i = 0;

int main()
{
    char* word;
    char null = '\0';
    word = malloc(sizeof(char) * 100);
    char transfer[100];
    printf("Enter a word to be translated into pig latin\n");
    gets(word);

    if(!is_vowel(word[i]))
    {
        transfer[i] = word[i];
        i++;
        while(!is_vowel(word[i]))
        {
            transfer[i] = word[i];
            i++;
        }
        for (int j = strlen(transfer), h = 0; j > strlen(word); j++,h++)
        {
            word[h] = word[j];
        }
        word[strlen(word)-1] = '\0';
        strcat(word,transfer);
        strcat(word,"ay");

        printf(word);
        
    }
    
}

int is_vowel(char letter)
{
    if(letter != 'a' &&
       letter != 'A' && 
       letter != 'e' &&
       letter != 'E' && 
       letter != 'i' && 
       letter != 'I' && 
       letter != 'o' && 
       letter != 'O' && 
       letter != 'u' && 
       letter != 'U')
    {
        return 0;
    }

    return 1;
}