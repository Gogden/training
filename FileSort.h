#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

void insertionSort(int* arr, int numElem);
void selectionSort(int* arr, int numElem);
void quickSortWrapper(int* arr, int numElem);
void mergeSortWrapper(int* arr, int numElem);
void quickSort(int* arr, int low, int high);
int partition (int* arr, int low, int high);
void mergeSort(int* arr, int left, int right);
void merge(int* arr, int left, int middle, int right);
void swap(int* a, int* b);
void dataPrint(int arr[], int n, clock_t t, bool time);
void* functionPicker(char flag);




//*********************For Acclaim Badge*************************//
//struct example
struct Computer {
    char cpuModel [20];
    char gpuModel [20];
    char serialNumber[20];
};
//linked list
struct ComputerNetwork {
    struct Computer comp;
    int key;
    struct ComputerNetwork *next;
};

